package com.example.instructionapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = findViewById(R.id.list_view);

        String[] siteStack = getResources().getStringArray(R.array.listItemsNames);
        final String[] listItemsNamesLinks = getResources().getStringArray(R.array.listItemsNamesLinks);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.my_list, siteStack);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                Intent intent = new Intent(MainActivity.this, WebActivity.class);
                intent.putExtra("link",listItemsNamesLinks[position]);
                startActivity(intent);
            }
        });
    }
}